#pragma once
#include "ClacIntegral.h"
class TrapezoidMethod : public CalcIntegral
{
public:
	TrapezoidMethod();
	TrapezoidMethod(int numOfPoints, double accuracy);

	double Calc(std::function<double(double)>& integrandExpression, double lowerBound, double upperBound) override;
};

