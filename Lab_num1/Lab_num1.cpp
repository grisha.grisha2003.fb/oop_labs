﻿#include <iostream>
#include "SimpsonMethod.h"
#include "TrapezoidMethod.h"

int main()
{
	setlocale(LC_ALL, "Rus");
	TrapezoidMethod integral1 = TrapezoidMethod(1000, 0.00001);
	SimpsonMethod integral2 = SimpsonMethod(1000, 0.00001);
	std::function<double(double)>  integrandExpression = [](double x)
	{
		return (3 * x * x + 2 * x);
	};

	double lowerBound = 0;
	double upperBound = 1;
	double trapezoidalResult = integral1.Calc(integrandExpression, lowerBound, upperBound);
	double simpsonResult = integral2.Calc(integrandExpression, lowerBound, upperBound);

	std::cout << "Вычисление методом трапеций: " << trapezoidalResult << std::endl;
	std::cout << "Вычисление методом Симпсона: " << simpsonResult << std::endl;
	return 0;
}

