#include "TrapezoidMethod.h"

TrapezoidMethod::TrapezoidMethod() : CalcIntegral()
{
}

TrapezoidMethod::TrapezoidMethod(int numOfPoints, double accuracy) : CalcIntegral(numOfPoints, accuracy)
{
}

double TrapezoidMethod::Calc(std::function<double(double)>& integrandExpression, double lowerBound, double upperBound)
{
    const double width = (upperBound - lowerBound) / m_numOfPoints;

    double trapezoidalIntegral = 0;
    for (int step = 0; step < m_numOfPoints; step++) {
        const double x1 = lowerBound + step * width;
        const double x2 = lowerBound + (step + 1) * width;

        trapezoidalIntegral += 0.5 * (x2 - x1) * (integrandExpression(x1) + integrandExpression(x2));
    }
    return trapezoidalIntegral;
}
