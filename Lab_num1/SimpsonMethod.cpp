#include "SimpsonMethod.h"

SimpsonMethod::SimpsonMethod() : CalcIntegral()
{
}

SimpsonMethod::SimpsonMethod(int numOfPoints, double accuracy) : CalcIntegral(numOfPoints, accuracy)
{
}

double SimpsonMethod::Calc(std::function<double(double)>& integrandExpression, double lowerBound, double upperBound)
{
	const double width = (upperBound - lowerBound) / m_numOfPoints;

	double simpson_integral = 0;
	for (int step = 0; step < m_numOfPoints; step++)
		{
		const double x1 = lowerBound + step * width;
		const double x2 = lowerBound + (step + 1) * width;

		simpson_integral += (x2 - x1) / 6.0 * (integrandExpression(x1) + 4.0 * integrandExpression(0.5 * (x1 + x2)) + integrandExpression(x2));
		}
	return simpson_integral;
}
