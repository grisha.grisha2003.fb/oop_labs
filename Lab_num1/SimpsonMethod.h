#pragma once
#include "ClacIntegral.h"
class SimpsonMethod : public CalcIntegral
{
public:
	SimpsonMethod();
	SimpsonMethod(int numOfPoints, double accuracy);
	double Calc(std::function<double(double)>& integrandExpression, double lowerBound, double upperBound) override;
};

