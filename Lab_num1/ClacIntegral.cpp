#include "ClacIntegral.h"

CalcIntegral::CalcIntegral()
{
	m_numOfPoints = 0;
	m_accuracy = 0.f;
}

CalcIntegral::CalcIntegral(int numOfPoints, double accuracy)
{
	m_numOfPoints = numOfPoints;
	m_accuracy = accuracy;
}
