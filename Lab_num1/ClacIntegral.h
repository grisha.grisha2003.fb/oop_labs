#pragma once
#include <functional>

class CalcIntegral
{
public:

	CalcIntegral();
	CalcIntegral(int numOfPoints, double accuracy);

	virtual double Calc(std::function<double(double)>& integrandExpression, double lowerBound, double upperBound) = 0;

protected:
	int m_numOfPoints;
	double m_step;
	double m_accuracy;
};

