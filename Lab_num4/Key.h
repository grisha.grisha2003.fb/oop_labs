#pragma once

#include "IKey.h"
#include <vector>

class Key : public IKey
{
public:
	Key(std::string const& name);
	Key(Key const& other);
	std::string getName() const override;
	bool operator == (Key const& other) const;
private:
	std::string m_name;
};

template<>
struct std::hash<Key> {
	size_t operator() (const Key& arg) const {
		size_t h1 = hash<string>{}(arg.getName());
		return h1;
	}
};

template<>
struct std::hash<std::vector<Key>> {
	size_t operator() (std::vector<Key> const& args) const {
		size_t result = 0;
		for (int i = 0; i < args.size(); ++i)
		{
			size_t h = hash<string>{}(args[i].getName());
			result ^= h + 0x9e3779b9 + (result << 6) + (result >> 2);
		}
		return result;
	}
};