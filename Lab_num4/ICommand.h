#pragma once
#include <string>
#include "Key.h"

class ICommand
{
public:
	virtual void setAction(std::string const& action) = 0;
	virtual std::vector<Key> getKeys() const = 0;
	virtual std::string getAction() const = 0;
};
