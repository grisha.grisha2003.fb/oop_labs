#pragma once

#include "IKeyboard.h"

class Keyboard : public IKeyboard
{
public:
    Keyboard(Workflow workflow);
    void press(std::vector<Key> const& keys) override;
    void assignCombination(std::vector<Key> const& keys, const std::string& action) override;
    void undo() override;

private:
    Workflow m_workflow;
};
