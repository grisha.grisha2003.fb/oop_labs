#include "Command.h"

Command::Command()
	:m_keys({}), m_action("")
{
}

Command::Command(Key& key)
	:m_keys(std::vector<Key>{key}), m_action(key.getName())
{
}

Command::Command(std::vector<Key> const& keys, std::string const& action)
	:m_keys(keys), m_action(action)
{
}

void Command::setAction(std::string const& action)
{
	m_action = action;
}

std::vector<Key> Command::getKeys() const
{
	return m_keys;
}

std::string Command::getAction() const
{
	return m_action;
}