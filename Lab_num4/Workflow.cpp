#include "Workflow.h"

Workflow::Workflow()
    :m_text("")
{
};

void Workflow::update(std::vector<Key> const& keys)
{
    std::string keysName;
    for (auto key : keys)
        keysName += key.getName() + "+";
    keysName.pop_back();
    m_commandSequence.emplace_back(Command(keys, (keys.size() > 1 ? "pressKeys(" : "pressKey(") + keysName + ")"));
    updateText(keys);
    system("cls");
    showData();
    startTable();
    showActions();
    endTable();
    std::this_thread::sleep_for(std::chrono::seconds(3));
}

void Workflow::updateText(std::vector<Key> const& keys)
{
    while (m_text.find("\n") != std::string::npos)
        m_text = m_text.substr(0, m_text.find("\n"));
    if (keys[0].getName() == "undo")
        undo();
    else
    {
        if (keys.size() == 1)
            m_text += keys[0].getName();
        else if (keys.size() == 2 && keys[0].getName() == "Shift")
            m_text.push_back(keys[1].getName()[0] - 32);
        else
            m_text += m_commands[keys];
    }
}

void Workflow::assignCombination(std::vector<Key> const& keys, const std::string& action)
{
    m_commands[keys] = "\n" + action;
}

void Workflow::startTable()
{
    std::cout << "*---------------------*------------------------------------*\n";
    std::cout << "| Key                 |     Log                            |\n";
    std::cout << "*---------------------*------------------------------------*\n";
}

void Workflow::showActions()
{
    for (auto const& command : m_commandSequence)
    {
        std::string pressedKeys;
        for (auto const& key : command.getKeys())
            pressedKeys += key.getName() + "+";
        pressedKeys.pop_back();
        std::cout << "| " << std::left << std::setw(19) << pressedKeys << " | " << std::setw(34) << command.getAction() << " |\n";
    }
}

void Workflow::showData()
{
    std::cout << m_text << std::endl;
}

void Workflow::endTable()
{
    std::cout << "*---------------------*------------------------------------*\n";
}

void Workflow::undo()
{
    if (m_commandSequence.empty())
        return;

    int iter = m_commandSequence.size() - 1;
    auto lastCommand = m_commandSequence[iter];
    while (lastCommand.getKeys()[0].getName() == "undo")
    {
        --iter;
        lastCommand = m_commandSequence[iter];
    }

    m_commandSequence.emplace_back(Command(std::vector<Key>{ Key("undo") }, "undo " + lastCommand.getAction()));
    if (lastCommand.getKeys()[0].getName().length() == 1 || lastCommand.getKeys()[0].getName() == "Shift")
        m_text.pop_back();
    else
        m_text = m_text.substr(0, m_text.find("\n"));

    std::this_thread::sleep_for(std::chrono::seconds(3));
}