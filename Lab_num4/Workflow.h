#pragma once

#include <unordered_map>
#include <unordered_set>

#include "IWorkflow.h"
#include "Command.h"

class Workflow : public IWorkflow
{
public:
    Workflow();
    void update(std::vector<Key> const& keys) override;
    void updateText(std::vector<Key> const& keys) override;
    void assignCombination(std::vector<Key> const& keys, const std::string& action);
    void startTable();
    void showActions() override;
    void showData();
    void endTable();
    void undo();
private:
    std::string m_text;
    std::unordered_map<std::vector<Key>, std::string> m_commands;
    std::vector<Command> m_commandSequence;
};
