#pragma once

#include <vector>

#include "ICommand.h"

class Command : public ICommand
{
public:
	Command();
	Command(Key& key);
	Command(std::vector<Key> const& keys, std::string const& action);

	void setAction(std::string const& action) override;
	std::vector<Key> getKeys() const override;
	std::string getAction() const override;
private:
	std::vector <Key> m_keys;
	std::string m_action;
};