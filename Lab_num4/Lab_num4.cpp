﻿#include "Keyboard.h"
#include "Key.h"

int main() {
    Workflow workFlow;
    Keyboard keyboard = Keyboard(workFlow);

    keyboard.assignCombination(std::vector<Key>{Key("Fn"), Key("F7")}, "Sound volume has been increased!");
    keyboard.assignCombination(std::vector<Key>{Key("Fn"), Key("F8")}, "Sound volume has been decreased!");

    keyboard.press(std::vector<Key>{Key("a")});
    keyboard.press(std::vector<Key>{Key("a")});
    keyboard.undo();

    keyboard.press(std::vector<Key>{Key("b")});
    keyboard.press(std::vector<Key>{Key("Fn"), Key("F7")});
    keyboard.undo();

    keyboard.press(std::vector<Key>{Key("Fn"), Key("F8")});
    keyboard.press(std::vector<Key>{Key("Shift"), Key("b")});

    keyboard.assignCombination(std::vector<Key>{Key("Fn"), Key("F8")}, "New action for combination");
    keyboard.press(std::vector<Key>{Key("Fn"), Key("F8")});

    return 0;
}