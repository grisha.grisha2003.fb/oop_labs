#include "Keyboard.h"

Keyboard::Keyboard(Workflow workflow)
    :m_workflow(workflow)
{
}

void Keyboard::press(std::vector<Key> const& keys)
{
    m_workflow.update(keys);
}

void Keyboard::assignCombination(std::vector<Key> const& keys, const std::string& action)
{
    m_workflow.assignCombination(keys, action);
}

void Keyboard::undo()
{
    m_workflow.undo();
}
