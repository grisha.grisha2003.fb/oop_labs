#pragma once
#include <vector>
#include <iostream>

class IKeyboard
{
public:
    virtual void pressKey(const std::string& key) = 0;
    virtual void pressKeys(const std::string& keys) = 0;
    virtual void undo() = 0;
};
