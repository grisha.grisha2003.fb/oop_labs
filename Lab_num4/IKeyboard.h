#pragma once
#include <vector>
#include <iostream>
#include "Workflow.h"

class IKeyboard
{
public:
    virtual void press(std::vector<Key> const& keys) = 0;
    virtual void assignCombination(std::vector<Key> const& keys, const std::string& action) = 0;
    virtual void undo() = 0;
};