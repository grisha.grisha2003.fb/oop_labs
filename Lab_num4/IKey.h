#pragma once
#include <string>

class IKey
{
public:
	virtual std::string getName() const = 0;
};