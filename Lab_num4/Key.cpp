#include "Key.h"

Key::Key(std::string const& name)
    :m_name(name)
{
}

Key::Key(Key const& other)
{
    m_name = other.m_name;
}

std::string Key::getName() const
{
    return m_name;
}

bool Key::operator==(Key const& other) const
{
    return m_name == other.m_name;
}