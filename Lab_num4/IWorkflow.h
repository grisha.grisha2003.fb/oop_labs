#pragma once
#include <vector>
#include <iostream>
#include <iomanip>
#include <thread>
#include <chrono>
#include "Key.h"

class IWorkflow
{
public:
    virtual void update(std::vector<Key> const& keys) = 0;
    virtual void updateText(std::vector<Key> const& keys) = 0;
    virtual void showActions() = 0;
};