﻿#include <iostream>
#include "UserManager.h"

int main()
{
    UserRepository<User> repo("file.txt");
    User u1("Pereverzev", "123", "Grigoriy", 0);
    repo.Add(u1);

    UserManager um(repo);
    um.SignIn(u1);
    std::cout << std::boolalpha << "Somebody's authorized: " << um.isAuthorized() << std::endl;

    return 0;
}

