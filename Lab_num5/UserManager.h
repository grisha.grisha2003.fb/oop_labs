#pragma once
#include "IUserManager.h"
#include "UserRepository.h"

class UserManager : public IUserManager
{
public:
	UserManager(UserRepository<User>& uRepo) :m_uRepo(uRepo), m_authorizedUser(nullptr) {};
	~UserManager();
	void SignIn(User user) override;
	void SignOut() override;
	bool isAuthorized() override;
private:
	UserRepository<User>& m_uRepo;
	User* m_authorizedUser;
};

