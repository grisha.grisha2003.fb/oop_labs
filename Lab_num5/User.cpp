#include "User.h"

User::User(std::string userData, long int id)
	:m_login(""), m_password(""), m_name(""), m_id(id)
{
	short firstSeparator = userData.find(' ');
	short secondSeparator = userData.find(' ', firstSeparator + 1);
	if (firstSeparator != std::string::npos && secondSeparator != std::string::npos)
	{
		m_login = userData.substr(0, firstSeparator);
		m_password = userData.substr(firstSeparator + 1, secondSeparator - firstSeparator - 1);
		m_name = userData.substr(secondSeparator + 1, userData.length() - secondSeparator - 1);
	}
}
