#pragma once
#include <string>

class User
{
public:
	User() :m_login(""), m_password(""), m_name(""), m_id(0) {};

	User(std::string login, std::string password, std::string name, long int id)
		:m_login(login), m_password(password), m_name(name), m_id(id) {};

	User(std::string userData, long int id);
	std::string getLogin() { return m_login; }
	std::string getPassword() { return m_password; }
	std::string getName() { return m_name; }
	int getId() { return m_id; }
private:
	long int m_id;
	std::string m_login;
	std::string m_password;
	std::string m_name;
};

