#pragma once
#include "Vector3D.h"
template<typename T>
std::vector<std::vector<T>> Vector3D<T>::getValuesX(int X)
{
	std::vector<std::vector<T>> subArray(m_dimY, std::vector<T>(m_dimZ, T(0)));
	if (X < m_dimX)
		for (int j = 0; j < m_dimY; j++)
			for (int k = 0; k < m_dimZ; k++)
				subArray[j][k] = m_arr[X * m_dimX * m_dimX + j * m_dimY + k];
	return subArray;
}

template<typename T>
std::vector<std::vector<T>> Vector3D<T>::getValuesY(int Y)
{
	std::vector<std::vector<T>> subArray(m_dimX, std::vector<T>(m_dimZ, T(0)));
	if (Y < m_dimY)
		for (int i = 0; i < m_dimY; i++)
			for (int k = 0; k < m_dimZ; k++)
				subArray[i][k] = m_arr[i * m_dimX * m_dimX + Y * m_dimY + k];
	return subArray;
}

template<typename T>
std::vector<std::vector<T>> Vector3D<T>::getValuesZ(int Z)
{
	std::vector<std::vector<T>> subArray(m_dimX, std::vector<T>(m_dimY, T(0)));
	if (Z < m_dimZ)
		for (int i = 0; i < m_dimY; i++)
			for (int j = 0; j < m_dimZ; j++)
				subArray[i][j] = m_arr[i * m_dimX * m_dimX + j * m_dimY + Z];
	return subArray;
}

template<typename T>
std::vector<T> Vector3D<T>::getValuesXY(int X, int Y)
{
	std::vector<T> subArray = std::vector<T>(m_dimZ, T(0));
	if (X < m_dimX && Y < m_dimY)
		for (int k = 0; k < m_dimZ; k++)
			subArray[k] = m_arr[X * m_dimX * m_dimX + Y * m_dimY + k];
	return subArray;
}

template<typename T>
std::vector<T> Vector3D<T>::getValuesXZ(int X, int Z)
{
	std::vector<T> subArray = std::vector<T>(m_dimY, T(0));
	if (X < m_dimX && Z < m_dimZ)
		for (int j = 0; j < m_dimY; j++)
			subArray[j] = m_arr[X * m_dimX * m_dimX + j * m_dimZ + Z];
	return subArray;
}

template<typename T>
std::vector<T> Vector3D<T>::getValuesYZ(int Y, int Z)
{
	std::vector<T> subArray = std::vector<T>(m_dimX, T(0));
	if (Y < m_dimY && Z < m_dimZ)
		for (int i = 0; i < m_dimX; i++)
			subArray[i] = m_arr[i * m_dimX * m_dimX + Y * m_dimY + Z];
	return subArray;
}

template<typename T>
void Vector3D<T>::setValuesX(int X, std::vector<std::vector<T>> subArray)
{
	if (subArray.size() == m_dimY && subArray[0].size() == m_dimZ)
	{
		for (int j = 0; j < m_dimY; j++)
			for (int k = 0; k < m_dimZ; k++)
				m_arr[X * m_dimX * m_dimX + j * m_dimY + k] = subArray[j][k];
	}
}

template<typename T>
void Vector3D<T>::setValuesY(int Y, std::vector<std::vector<T>> subArray)
{
	if (subArray.size() == m_dimX && subArray[0].size() == m_dimZ)
	{
		for (int i = 0; i < m_dimX; i++)
			for (int k = 0; k < m_dimZ; k++)
				m_arr[i * m_dimX * m_dimX + Y * m_dimY + k] = subArray[i][k];
	}
}

template<typename T>
void Vector3D<T>::setValuesZ(int Z, std::vector<std::vector<T>> subArray)
{
	if (subArray.size() == m_dimX && subArray[0].size() == m_dimY)
	{
		for (int i = 0; i < m_dimX; i++)
			for (int j = 0; j < m_dimY; j++)
				m_arr[i * m_dimX * m_dimX + j * m_dimX + Z] = subArray[i][j];
	}
}

template<typename T>
void Vector3D<T>::setValuesXY(int X, int Y, std::vector<T> subArray)
{
	if (subArray.size() == m_dimZ)
	{
		for (int k = 0; k < m_dimZ; k++)
			m_arr[X * m_dimX * m_dimX + Y * m_dimY + k] = subArray[k];
	}
}

template<typename T>
void Vector3D<T>::setValuesXZ(int X, int Z, std::vector<T> subArray)
{
	if (subArray.size() == m_dimY)
	{
		for (int j = 0; j < m_dimY; j++)
			m_arr[X * m_dimX * m_dimX + j * m_dimY + Z] = subArray[j];
	}
}

template<typename T>
void Vector3D<T>::setValuesYZ(int Y, int Z, std::vector<T> subArray)
{
	if (subArray.size() == m_dimX)
	{
		for (int i = 0; i < m_dimX; i++)
			m_arr[i * m_dimX * m_dimX + Y * m_dimY + Z] = subArray[i];
	}
}

template<typename T>
void Vector3D<T>::ones()
{
	for (int i = 0; i < m_arrSize; i++)
		m_arr[i] = T(1);
}

template<typename T>
void Vector3D<T>::zeros()
{
	for (int i = 0; i < m_arrSize; i++)
		m_arr[i] = T(0);
}

template<typename T>
void Vector3D<T>::fill(T sample)
{
	for (int i = 0; i < m_arrSize; i++)
		m_arr[i] = sample;
}