#pragma once
#include <vector>
#include <stdexcept>

template<typename T>
class Vector3D
{
public:
	Vector3D() : m_arr(0), m_arrSize(0), m_dimX(0), m_dimY(0), m_dimZ(0)
	{};

	Vector3D(int dimX, int dimY, int dimZ) : m_arr(nullptr), m_dimX(dimX), m_dimY(dimY), m_dimZ(dimZ)
	{
		m_arr = new T[m_dimX * m_dimY * m_dimZ];
		m_arrSize = m_dimX * m_dimY * m_dimZ;
	};

	~Vector3D()
	{
		delete m_arr;
	}

	T& operator()(int X, int Y, int Z)
	{
		if (X < m_dimX && Y < m_dimY && Z < m_dimZ)
			return m_arr[X * m_dimX + Y * m_dimY + Z * m_dimZ];
		else
			throw std::runtime_error("Error: Invalid index");
	}

	std::vector<std::vector<T>> getValuesX(int X);
	std::vector<std::vector<T>> getValuesY(int Y);
	std::vector<std::vector<T>> getValuesZ(int Z);
	std::vector<T> getValuesXY(int X, int Y);
	std::vector<T> getValuesXZ(int X, int Z);
	std::vector<T> getValuesYZ(int Y, int Z);

	void setValuesX(int X, std::vector<std::vector<T>> subArray);
	void setValuesY(int Y, std::vector<std::vector<T>> subArray);
	void setValuesZ(int Z, std::vector<std::vector<T>> subArray);
	void setValuesXY(int X, int Y, std::vector<T> subArray);
	void setValuesXZ(int X, int Z, std::vector<T> subArray);
	void setValuesYZ(int Y, int Z, std::vector<T> subArray);

	void ones();
	void zeros();
	void fill(T sample);
	


private:
	T* m_arr;
	int m_arrSize;
	int m_dimX;
	int m_dimY;
	int m_dimZ;
};


#include "Vector3D_impl.h"