﻿#include <iostream>
#include "Vector3D.h"

#define SPLIT std::cout << "----------------" << std::endl;

int main()
{
    setlocale(LC_ALL, "Rus");
    Vector3D<int> vec = Vector3D<int>(3, 3, 3);
   
    vec(0, 0, 0) = 1;
    vec(1, 1, 1) = 111;
    
    std::cout << vec(0, 0, 0) << " " << vec(1, 1, 1) << std::endl;
    SPLIT;

    vec.zeros();

    std::cout << vec(0, 0, 0) << " " << vec(1, 1, 1) << std::endl;
    SPLIT;

    std::vector<std::vector<int>> subArray = { {1,2,3}, {4,5,6}, {7,8,9} };
  
    for (std::vector<int> vector : vec.getValuesX(0)) //Срез по X 0
    {
        for (int i = 0; i < vector.size(); i++)
        {
            std::cout << vector[i] << " ";
        }
        std::cout << std::endl;
    };
    SPLIT;
    for (std::vector<int> vector : vec.getValuesY(0)) //Срез по Y 0
    {
        for (int i = 0; i < vector.size(); i++)
        {
            std::cout << vector[i] << " ";
        }
        std::cout << std::endl;
    };
    SPLIT;

    for (std::vector<int> vector : vec.getValuesZ(0)) //Срез по Z 0
    {
        for (int i = 0; i < vector.size(); i++)
        {
            std::cout << vector[i] << " ";
        }
        std::cout << std::endl;
    };
    SPLIT;

    std::cout << "Зададим значения" << std::endl;
    
    vec.setValuesX(0, { {1,2,3}, {4,5,6}, {7,8,9} });
    for (std::vector<int> vector : vec.getValuesX(0)) //Срез по X 0
    {
        for (int i = 0; i < vector.size(); i++)
        {
            std::cout << vector[i] << " ";
        }
        std::cout << std::endl;
    };
    SPLIT;

    std::cout << "Двойной срез" << std::endl;
    std::cout << "По Х=0 Y=0" << std::endl;
    std::vector<int> vector = vec.getValuesXY(0, 0);
    for (int i = 0; i < vector.size(); i++)
    {
        std::cout << vector[i] << " ";
    }
    std::cout << std::endl;

    std::cout << "По Х=0 Y=1" << std::endl;
    std::vector<int> vector2 = vec.getValuesXY(0, 1);
    for (int i = 0; i < vector2.size(); i++)
    {
        std::cout << vector2[i] << " ";
    }
    std::cout << std::endl;

    std::cout << "По Х=0 Y=2" << std::endl;
    std::vector<int> vector3 = vec.getValuesXY(0, 2);
    for (int i = 0; i < vector3.size(); i++)
    {
        std::cout << vector3[i] << " ";
    }
    std::cout << std::endl;
    SPLIT;

    vec.fill(5);
    std::vector<int> output = vec.getValuesYZ(0, 0);
    for (int i = 0; i < output.size(); i++)
        std::cout << output[i];

    
    return 0;
}


