#pragma once
#include <iostream>
#include "MyVector.h"
#include <vector>
class UserInterface
{
public:
	UserInterface();
	void start();
	void update();
	void showVectors();
	void addVectors(int num);
	void doOperation();

private:
	std::vector<MyVector> vectors;

};

