﻿#include "UserInterface.h"

UserInterface::UserInterface():vectors(0)
{
}

void UserInterface::start()
{
	setlocale(LC_ALL, "Rus");
	update();
}

void UserInterface::update()
{
	int currentOperation = 0;
	while (currentOperation != -1)
	{
		std::cout << "\n\nВыберете оепрацию:\n"
			<< "1 - Выполнить арефметическую операцию\n"
			<< "2 - Получить единичный вектор\n"
			<< "3 - Получить длину вектора\n"
			<< "4 - Проверить векторы на коллинеарность\n"
			<< "5 - Проверить векторы на компланарность\n"
			<< "6 - Получить расстояние между векторами\n"
			<< "7 - Получить угол между векторами\n"
			<< "8 - Закрыть программу\n";
		std::cin >> currentOperation;
		switch (currentOperation)
		{
		case 1:
			doOperation();
			break;
		case 2:
			addVectors(1);
			vectors[0].normalize();
			std::cout << "Единичный вектор:\t";
			vectors[0].print();
			break;
		case 3:
			addVectors(1);
			std::cout << "Длина вектора:\t" << vectors[0].getLength() << std::endl;
			break;
		case 4:
			addVectors(2);
			std::cout << "Заданные векторы " << (vectors[0].checkIfCollinear(vectors[1]) ? "коллинеарны" : "не коллинеарны") << std::endl;
			break;
		case 5:
			addVectors(3);
			std::cout << "Заданные векторы " << (vectors[0].checkIfComplanar(vectors[1], vectors[2]) ? "компланарны" : "не компланарны") << std::endl;
			break;
		case 6:
			addVectors(2);
			std::cout << "Расстояние между векторами:\t" << vectors[0].findDistanceBetweenVectors(vectors[1]) << std::endl;
			break;
		case 7:
			addVectors(2);
			std::cout << "Угол между векторами:\t" << vectors[0].findAngleBetweenVectors(vectors[1]) << std::endl;
			break;
		case 8:
			system("cls");
			break;
		case -1:
			break;
		default:
			std::cout << "Введите команду из списка!" << std::endl;
			break;
		}
		vectors.clear();
	}
}

void UserInterface::showVectors()
{
	std::cout << "\nСуществующие вектора: " << std::endl << std::endl;
	for (int i = 0; i < vectors.size(); i++)
	{
		std::cout << "Вектор" << i;
		vectors[i].print();
	}
	std::cout << std::endl;
}

void UserInterface::addVectors(int number)
{
	while (number > 0)
	{
		double x, y, z;
		std::cout << "Введите координаты вектора (x y z):\n";
		std::cin >> x >> y >> z;
		vectors.push_back(MyVector(x, y, z));
		number--;
	}
	showVectors();
}


void UserInterface::doOperation()
{
	char currentOperation = ' ';
	std::cout << "\nВыберете операцию:\n"
		<< "+\tСложить\n"
		<< "-\tВычесть\n"
		<< "*\tСкалярное произведение\n"
		<< "1\tПротивоположный вектор\n"
		<< "2\tВекторное произведение\n"
		<< "3\tСмешаное произведение\n\n";
	std::cin >> currentOperation;
	switch (currentOperation)
	{
	case '+':
		addVectors(2);
		std::cout << "Сумма векторов равна: ";
		(vectors[0] + vectors[1]).print();
		break;
	case '-':
		addVectors(2);
		std::cout << "Разность векторов равна: ";
		(vectors[0] - vectors[1]).print();
		break;
	case '*':
		addVectors(2);
		std::cout << "Скалярное произведение векторов равно:\t" << (vectors[0] * vectors[1]) << std::endl;
		break;
	case '1':
		addVectors(1);
		std::cout << "Противоположный вектор: ";
		(-vectors[0]).print();
		break;
	case '2':
		addVectors(2);
		std::cout << "Векторное произведение равняется: ";
		vectors[0].vectorMultiplication(vectors[1]).print();
		break;
	case '3':
		addVectors(3);
		std::cout << "Смешаное произведение равно:\t" << vectors[0].mixedMultiplication(vectors[1], vectors[2]) << std::endl;;
		break;
	default:
		std::cout << "Некорректная команда, выберете из списка!" << std::endl;
	}
}