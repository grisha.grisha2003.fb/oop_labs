#pragma once
#include "Point.h"
#include <math.h>
class MyVector
{
public:

	MyVector();
	MyVector(double x, double y, double z);
	MyVector(Point p1, Point p2);

	bool operator == (const MyVector& secVector) const;
	MyVector operator + (const MyVector& secVector) const;
	MyVector operator - (const MyVector& secVector) const;
	MyVector operator - () const;
	double operator * (const MyVector& secVector) const;

	double getLength() const;
	MyVector normalize() const;

	MyVector vectorMultiplication(const MyVector& second);
	double mixedMultiplication(const MyVector& second, const MyVector& third);
	bool checkIfCollinear(const MyVector& second);
	bool checkIfComplanar(const MyVector& second, const MyVector& third);
	double findDistanceBetweenVectors(const MyVector& second);
	double findAngleBetweenVectors(MyVector& second);

	void print();

private:

	double x;
	double y;
	double z;

};

