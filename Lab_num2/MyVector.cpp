#include "MyVector.h"
#include <iostream>

MyVector::MyVector()
{
	x = y = z = 0;
}

MyVector::MyVector(double t_x, double t_y, double t_z)
{
	x = t_x;
	y = t_y;
	z = t_z;
}

MyVector::MyVector(Point p1, Point p2)
{
	x = p2.p_x - p1.p_x;
	y = p2.p_y - p1.p_y;
	z = p2.p_z - p1.p_z;
}

bool MyVector::operator==(const MyVector& secVector) const
{
	return x == secVector.x && y == secVector.y && z == secVector.z;
}

MyVector MyVector::operator+(const MyVector& secVector) const
{
	double t_x = this->x + secVector.x;
	double t_y = this->y + secVector.y;
	double t_z = this->z + secVector.z;
	return MyVector(t_x, t_y, t_z);
}

MyVector MyVector::operator-(const MyVector& secVector) const
{
	double t_x = this->x - secVector.x;
	double t_y = this->y - secVector.y;
	double t_z = this->z - secVector.z;
	return MyVector(t_x, t_y, t_z);
}

MyVector MyVector::operator-() const
{
	return MyVector(-x, -y, -z);
}

double MyVector::operator*(const MyVector& secVector) const
{
	return (x * secVector.x + y * secVector.y + z * secVector.z);
}

double MyVector::getLength() const
{
	return sqrt(x * x + y * y + z * z);
}

MyVector MyVector::normalize() const
{
	double t_x = x / this->getLength();
	double t_y = y / this->getLength();
	double t_z = z / this->getLength();
	return MyVector(t_x, t_y, t_z);
}

MyVector MyVector::vectorMultiplication(const MyVector& second)
{
	double t_x = y * second.z - z * second.y;
	double t_y = z * second.x - x * second.z;
	double t_z = x * second.y - y * second.x;
	return MyVector(t_x, t_y, t_z);
}

double MyVector::mixedMultiplication(const MyVector& second, const MyVector& third)
{
	return this->vectorMultiplication(second) * third;
}

bool MyVector::checkIfCollinear(const MyVector& second)
{
	return this->normalize() == second.normalize() || this->normalize() == -second.normalize();
}

bool MyVector::checkIfComplanar(const MyVector& second, const MyVector& third)
{
	return (this->mixedMultiplication(second, third) == 0);
}

double MyVector::findDistanceBetweenVectors(const MyVector& second)
{
	return (this->operator - (second)).getLength();
}

double MyVector::findAngleBetweenVectors(MyVector& second)
{
	return acos((this->operator* (second)) / (this->getLength() * second.getLength())) * 180.0 / 3.14159265;
}

void MyVector::print()
{
	std::cout << "\t" << x << "\t" << y << "\t" << z << std::endl;
}
